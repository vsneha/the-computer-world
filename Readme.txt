A dynamic webpage where you can check you Balance, Pay, work and buy laptops. 

Features:
HTML, CSS , Bootstrap ,vanilla JavaScript and fetching API.

Development environment:
Make sure you have installed at least the following tools:
• A text editor of your choice (Visual Studio Code recommended)
• Live Server VS add-on
• You will also use your browser’s Developer Tools for testing and debugging.

The Functionality of the Website:
.The Aim of the project is to Buy a Computer.
.The user can see this current balance account.
.The user can earn money by clicking on the Work button, by which his pay increases.
.The amount in this pay can be transferd to his bank account by using Bank amount.
.The user has the option to take loan from the bank.
.When the user has the Loan then 10% of this pay amount will be payed to the loan.
.The user can also Repay the whole Loan amount buy clicking on Repay Loan.
.The user can select the options which are fetched from an API to buy the computer.
.The Image , Price , Title, Description and Features of the computer are dispalyed.

Can check the Website : https://vsneha.gitlab.io/the-computer-world/
