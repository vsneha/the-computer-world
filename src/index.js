// fetching all the necessary key values pairs from the API
const computersElement = document.getElementById("computers");
const featuresElement = document.getElementById("features");
const imageElement = document.getElementById("computerImage");
const titleElement = document.getElementById("computerTitle");
const textElement = document.getElementById("computerText");
const priceElement = document.getElementById("computerPrice");
const buttonElement = document.getElementById("buyComputerButton");

//  To get Bank loans
const loanElement = document.getElementById("getLoanButton");
const currentBankBalance = document.getElementById("currentBankBalance");
const loanAmountTaken = document.getElementById("loanAmountTaken");

// Work and Pay
const bank = document.getElementById("bank");
const payElement = document.getElementById("pay")
const repayFullLoan = document.getElementById("repayFullLoan");
const workElement = document.getElementById("increasePay");

// API from where the computers are fetched
const API_ROOT = "https://noroff-komputer-store-api.herokuapp.com/";
let computers = [];
let balance = 0.0; 
let pay = 0.0; 
let isLoanTaken = false;
let totalLoantaken = 0;
let selectedComputerIndex = 0;

// fetching the required laptops information by selecting the titles of the object
fetch(API_ROOT + "computers")
  .then((response) => response.json())
  .then((data) => (computers = data))
  .then((computers) => addComputersToBuy(computers));

const addComputersToBuy = (computers) => {
  computers.forEach((element, index) => {
    addComputerToBuy(element)
    console.log("s" + index);
    handleComputerChangeFirst()
  });
};
const addComputerToBuy = (computer) => {
  const computerElement = document.createElement("option");
  computerElement.appendChild(document.createTextNode(computer.title));
  computersElement.appendChild(computerElement);
};
// Displaying only first three features of the specs , the image , title , description and price of laptops
const handleComputerChange = (event) => {
  selectedComputerIndex = event.target.selectedIndex;
  const selectedComputer = computers[event.target.selectedIndex];
  let ul = document.getElementById("features");
  ul.innerHTML = "";
  selectedComputer.specs.forEach((element, index) => {
    if (index < 3) {
      let li = document.createElement("li");
      li.appendChild(document.createTextNode(element));
      ul.appendChild(li);
    }
  });
  titleElement.innerHTML = selectedComputer.title;
  textElement.innerHTML = selectedComputer.description;
  priceElement.innerHTML = selectedComputer.price + "DK";
  let customImageUrl = API_ROOT + selectedComputer.image;
  imageElement.src = customImageUrl;
  let defaultImage = document.createAttribute("onerror"); // Default Image selection
  defaultImage.value = "this.src='./images/defaultImage.jpg'";
  imageElement.setAttributeNode(defaultImage);

};

// Displays the website with a computer to buy already with its  image, price featurse etc..
const handleComputerChangeFirst = () => {
  console.log("s" + "selectedComputer");
 const selectedComputer = computers[0];
  let ul = document.getElementById("features");
  ul.innerHTML = "";
  selectedComputer.specs.forEach((element, index) => {
    if (index < 3) {
      let li = document.createElement("li");
      li.appendChild(document.createTextNode(element));
      ul.appendChild(li);
    }
  });
  let customImageUrl = API_ROOT + selectedComputer.image;
  imageElement.src = customImageUrl;
  titleElement.innerHTML = selectedComputer.title;
  textElement.innerHTML = selectedComputer.description;
  priceElement.innerHTML = selectedComputer.price + "DK";
};

// to get a Loan
const handleLoanChange = () => {
  if(balance > 0) {
  const loanAmount = prompt("Please enter the amount you wish to take loan: ");
  const maximumLoanAllowed = parseFloat(balance) * 2; // only double the balance amount is allowed
  if (isLoanTaken) {
    alert("Already the loan is taken");
  } else {
    if (loanAmount > maximumLoanAllowed) {
      alert("You cannot get a loan more than double of your bank balance");
    } else if (loanAmount < 0) {
      alert(" You have entered a negitive number");
    } else if (isNaN(loanAmount)) {
      alert(" You have must enter a number");
    }
    else {
      alert(`the Loan amount you get is :  ${loanAmount}`);
      loanAmountTaken.style.display = "block";
      repayFullLoan.style.display = "block";
      balanceAmount = parseFloat(loanAmount) + balance;
      currentBankBalance.innerHTML = "Current Balance : " + balanceAmount;
      totalLoantaken = totalLoantaken + loanAmount;
      loanAmountTaken.innerHTML = "Your Loan amount: " + loanAmount;
    }
  }
  isLoanTaken = true;
} 
else  {
  alert(`Your balance is ${balance} , Go work to get a loan!`);
}
};
// Bank button
handleBank = () => {
  if (totalLoantaken > 0) {
// If the loan amount is less than the pay 
    let deductedValue = (10/100)*pay; 
    if (totalLoantaken < deductedValue) {
      let remainingAmount = deductedValue  - totalLoantaken ;
      loanAmountTaken.innerHTML = "Your Loan amount: " + 0;
      balance = balance + remainingAmount;
      currentBankBalance.innerHTML = "Current Balance : " + balance;
    } else {
      let remainingAmount = pay  - deductedValue;
      totalLoantaken = totalLoantaken - deductedValue;
      loanAmountTaken.innerHTML = "Your Loan amount: " + totalLoantaken;
      balance = balance + remainingAmount;
      currentBankBalance.innerHTML = "Current Balance : " + balance;
    }  
  }
  else {
    balance = balance + pay;
  }
  pay = 0;
  payElement.innerHTML = "Pay :" + pay;
  currentBankBalance.innerHTML = "Current Balance : " + balance;
}
// the pay increases by 100 each timed it is clicked
handlework = () => {
  pay = pay + 100;
  payElement.innerHTML = "Pay :" + pay;
}
//the repayLoan Amount
handleRepayLoanAmount = () => {
  if (pay > totalLoantaken) {
    let remainingPay = pay-totalLoantaken;
    totalLoantaken = 0;
    pay = remainingPay
    loanAmountTaken.innerHTML = "Your Loan amount: " + 0;
    payElement.innerHTML = "Pay :" + remainingPay;

  }else if(pay < totalLoantaken){
    let remainingLoan = totalLoantaken - pay;
    totalLoantaken = remainingLoan;
    pay = 0;
    loanAmountTaken.innerHTML = "Your Loan amount: " + remainingLoan;
    payElement.innerHTML = "Pay :" + 0;
  }else {
    pay = 0;
    totalLoantaken = 0;
    loanAmountTaken.innerHTML = "Your Loan amount: " + 0;
    payElement.innerHTML = "Pay :" + 0;
  }
  if (totalLoantaken == 0) {
    isLoanTaken = false;
  }
}
// To Buy computer
const handleBuyComputer = () => {
  const selectedComputer = computers[selectedComputerIndex];
  const laptopPrice = selectedComputer.price;
  const bankBalance = balance;
  if (laptopPrice > bankBalance) {
    alert("you cannot afford the laptop!")    
  }else {
    let remainingMoney = bankBalance - laptopPrice;
    currentBankBalance.innerHTML = "Current Balance : " + remainingMoney;
    alert("Congratulations you are now the owner of the new laptop!");
  }
}
computersElement.addEventListener("change", handleComputerChange);
loanElement.addEventListener("click", handleLoanChange);
buttonElement.addEventListener("click", handleBuyComputer);
bank.addEventListener("click", handleBank)
workElement.addEventListener("click", handlework)
repayFullLoan.addEventListener("click", handleRepayLoanAmount)

const init = () => {
  currentBankBalance.innerHTML = "Current Balance : " + balance;
  payElement.innerHTML = "Pay :" + pay;
  loanAmountTaken.style.display = "none";
  repayFullLoan.style.display = "none";
};

init();
